﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace AppConsomeAPI
{
    public class ConsomeSync
    {
        public void GetAllEvents()
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Content-Type:application/json");
                client.Headers.Add("Accept:application/json");
                var resultado = client.DownloadString("https://localhost:44380/api/Friends");
                Console.WriteLine(Environment.NewLine + resultado);
                Console.ReadKey();
            }
        }
    }
}