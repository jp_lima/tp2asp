﻿using ApiFriends.Models;
using System;
<<<<<<< HEAD
using System.Collections.Generic;
using System.Data;
=======
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
>>>>>>> f3bdd6eac004b8796e0d474840da4ee4e800f95e
using System.Data.SqlClient;

namespace ApiFriends.Repository
{
    public class AmigoRepository
    {
        private String _connectionString;
        private SqlConnection _sqlConnection;

        public AmigoRepository()
        {
            _connectionString = Properties.Settings.Default.DbConnectionString;
            _sqlConnection = new SqlConnection(_connectionString);
        }

<<<<<<< HEAD
        public IEnumerable<Amigo> PegarTodos()
        {
            List<Amigo> amigos = new List<Amigo>();

            SqlCommand sqlCommand = new SqlCommand("PegarTodosAmigos", _sqlConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            try
            {
                _sqlConnection.Open();
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    Amigo amigo = new Amigo
                    {
                        Id = (int)reader["Id"],
                        Nome = reader["Nome"].ToString(),
                        SobreNome = reader["SobreNome"].ToString(),
                        Email = reader["Email"].ToString(),
                        Telefone = (int)reader["Telefone"],
                        Aniversario = (DateTime)reader["Aniversario"]
                    };
                    amigos.Add(amigo);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                _sqlConnection.Close();
            }

            return amigos;
        }

        public Amigo PegarUmAmigo(int id)
        {
            Amigo amigo = new Amigo();

            SqlCommand sqlCommand = new SqlCommand("PegarAmigo", _sqlConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            try
            {
                _sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue("@Id", id);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    reader.GetInt32(0);
                    reader.GetString(1);
                    reader.GetString(2);
                    reader.GetString(3);
                    reader.GetInt32(4);
                    reader.GetDateTime(5);
                }

                return amigo;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _sqlConnection.Close();
            }
            
        }

        public void Add(Amigo amigo)
        {
            SqlCommand sqlCommand = new SqlCommand("CriarAmigo", _sqlConnection)
=======
        public void Add(Amigo amigo)
        {
            SqlCommand sqlCommand = new SqlCommand("CreateFriend", _sqlConnection)
>>>>>>> f3bdd6eac004b8796e0d474840da4ee4e800f95e
            {
                CommandType = CommandType.StoredProcedure
            };

            try
            {
                _sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue("@Id", amigo.Id);
                sqlCommand.Parameters.AddWithValue("@Nome", amigo.Nome);
                sqlCommand.Parameters.AddWithValue("@SobreNome", amigo.SobreNome);
                sqlCommand.Parameters.AddWithValue("@Email", amigo.Email);
                sqlCommand.Parameters.AddWithValue("@Telefone", amigo.Telefone);
                sqlCommand.Parameters.AddWithValue("@Aniversario", amigo.Aniversario);
<<<<<<< HEAD
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
=======
                sqlCommand.ExecuteNonQueryAsync(); //Gustavo passa NonQuery
            }
            catch (Exception e)
>>>>>>> f3bdd6eac004b8796e0d474840da4ee4e800f95e
            {
            }
            finally
            {
                _sqlConnection.Close();
            }
        }

<<<<<<< HEAD
        public void Atualizar(Amigo amigo)
        {
            SqlCommand sqlCommand = new SqlCommand("AtualizarAmigo", _sqlConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            try
            {
                _sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue("@Id", amigo.Id);
                sqlCommand.Parameters.AddWithValue("@Nome", amigo.Nome);
                sqlCommand.Parameters.AddWithValue("@SobreNome", amigo.SobreNome);
                sqlCommand.Parameters.AddWithValue("@Email", amigo.Email);
                sqlCommand.Parameters.AddWithValue("@Telefone", amigo.Telefone);
                sqlCommand.Parameters.AddWithValue("@Aniversario", amigo.Aniversario);
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                _sqlConnection.Close();
            }
        }

        public void Deletar(Amigo amigo)
        {
            SqlCommand sqlCommand = new SqlCommand("DeletarAmigo", _sqlConnection)
=======
        public IEnumerable<Amigo> PegarTodos()
        {
            List<Amigo> amigos = new List<Amigo>();

            SqlCommand sqlCommand = new SqlCommand("GetAllFriends", _sqlConnection)
>>>>>>> f3bdd6eac004b8796e0d474840da4ee4e800f95e
            {
                CommandType = CommandType.StoredProcedure
            };

            try
            {
                _sqlConnection.Open();
<<<<<<< HEAD
                sqlCommand.Parameters.AddWithValue("@Id", amigo.Id);
                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
=======
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    Amigo amigo = new Amigo
                    {
                        Id = (int) reader["Id"],
                        Nome = reader["Nome"].ToString(),
                        SobreNome = reader["SobreNome"].ToString(),
                        Email = reader["Email"].ToString(),
                        Telefone = (int) reader["Telefone"],
                        Aniversario = (DateTime) reader["Aniversario"]
                    };
                    amigos.Add(amigo);

                }
            }
            catch (Exception e)
>>>>>>> f3bdd6eac004b8796e0d474840da4ee4e800f95e
            {
            }
            finally
            {
                _sqlConnection.Close();
            }
<<<<<<< HEAD
=======

            return amigos;
>>>>>>> f3bdd6eac004b8796e0d474840da4ee4e800f95e
        }
    }
}