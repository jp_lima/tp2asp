﻿using ApiFriends.Repository;
using ApiFriends.Models;
using ApiFriends.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace ApiFriends.Controllers
{
<<<<<<< HEAD
    //[BasicAuthenticationResolver]     //NÃO QUERO AUTENTICAÇÃO
    public class AmigoController : ApiController
    {
        private AmigoRepository amigoRepository = new AmigoRepository();
=======
    //[BasicAuthenticationResolver]
    public class AmigoController : ApiController
    {
        private AmigoRepository friendsContext = new AmigoRepository();
>>>>>>> f3bdd6eac004b8796e0d474840da4ee4e800f95e

        public IEnumerable<Amigo> Get()
        {
            return amigoRepository.PegarTodos();
        }

        public HttpResponseMessage GetById(int id)
        {
            var amg = amigoRepository.PegarUmAmigo(id);

            if (amg != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, amg);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Id: " + id.ToString() + " não encontrado");
            }

        }

        public HttpResponseMessage Post(Amigo amigo)
        {
            try
            {
<<<<<<< HEAD
                amigoRepository.Add(amigo);
                amigoRepository.SaveChanges();
=======
                friendsContext.Amigos.Add(amigo);
                friendsContext.SaveChanges();
>>>>>>> f3bdd6eac004b8796e0d474840da4ee4e800f95e

                var message = Request.CreateResponse(HttpStatusCode.Created, amigo);
                message.Headers.Location = new Uri(Request.RequestUri + amigo.Id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Put(int id, Amigo friend)
        {
            try
            {

                var fr = amigosContext.Amigos.FirstOrDefault(p => p.Id == id);

                if (fr == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Id: " + id.ToString() + " não encontrado");
                }
                else
                {
                    fr.Nome = friend.Nome;
                    fr.SobreNome = friend.SobreNome;
                    fr.Email = friend.Email;
                    fr.Telefone = friend.Telefone;
                    fr.Aniversario = friend.Aniversario;

                    amigosContext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, fr);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var fr = amigosContext.Amigos.FirstOrDefault(j => j.Id == id);
                if (fr != null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Id: " + id.ToString() + " não encontrado");
                }
                else
                {
                    amigosContext.Amigos.Remove(fr);
                    amigosContext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
