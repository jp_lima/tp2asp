﻿using ApiFriends.Models;
using System;
using System.Data.Entity;

namespace ApiFriends.Context
{
    public class AmigoContext : DbContext
    {
        public DbSet<Amigo> Amigos { get; set; }

        public AmigoContext()
            : base(Properties.Settings.Default.DbConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}