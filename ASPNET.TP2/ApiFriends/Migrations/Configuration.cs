using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace ApiFriends.Migrations
{
    

    internal sealed class Configuration : DbMigrationsConfiguration<Repository.AmigoRepository>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApiFriends.Repository.AmigoRepository context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
