﻿
using System;

namespace ApiFriends.Models
{
    public class Amigo
    {

        public int Id { get; set; }

        public string Nome { get; set; }

        public string SobreNome { get; set; }

        public string Email { get; set; }

        public int Telefone { get; set; }

        public DateTime Aniversario { get; set; }
    }
}